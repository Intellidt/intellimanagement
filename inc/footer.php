    <!-- Footer -->
        <footer class="mt-5 space">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <p class="footer-company">Intelli Management Consulting Corp.</p>
                        <p class="footer-address">130-7080 River Road, Richmond, BC, V6X 1X5 Canada
                        </p>
                    </div>
                    <div class="col-md-5">
                        <h5 class="footer-phone">Call us: +1 (778) 297-7108</h5>
                        <h6 class="footer-alegada">Intelli Management Consulting Corp. © 2020 - All rights
                            reserved.</h6>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
            <!--Scripts-->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="fontawesome/js/all.js"></script>

<script>
    $(function () {
        $(document).scroll(function () {
            var $nav = $(".navbar-fixed-top");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
    });

</script>

</body>
</html>