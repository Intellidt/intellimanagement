<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Intelli Management</title>
    <!--stylesheets-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.css">
</head>
<body>
<div class="wrapper">
<!--Header logo & Navigation Bar-->
    <div class="container-fluid">
        <div class="navbar-container">
            <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="index.php"><img src="images/logo_intelli.png" alt="Intelli Management"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item text-uppercase active">
                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-uppercase" href="#" id="designDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Design Solutions
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="designDropdown">
                                    <li><a class="dropdown-item" href="graphic_design.php">Graphic Design</a></li>
                                    <li><a class="dropdown-item" href="web_development.php">Web Development</a></li>
                                    <li><a class="dropdown-item" href="marketing_campaign.php">Marketing Campaign</a></li>
                                    <li><a class="dropdown-item" href="interior_design.php">Interior Design</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-uppercase" href="#" id="marketingDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Marketing
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="marketingDropdown">
                                    <li><a class="dropdown-item" href="case.php">Case Studies</a></li>
                                    <li><a class="dropdown-item" href="event.php">Event Planning</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="our_client.php">Our Clients</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-uppercase" href="contact.php">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>