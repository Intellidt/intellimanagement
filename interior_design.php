<?php
@include ("inc/header.php");
?>
<section class="interior-back">
    <div class="container">
        <div  class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h1>Design Solution</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <h2 class="color-title text-center my-5">Interior Design</h2>
    <div class ="row">
        <div class="col-md-6">
            <p class="text-left">
                When it comes to interior design, whether for a small or a large space, our interior designers understand that style, comfort, livability and sustainability are of the utmost important. We are experienced in interior designs for various projects, ranging from classical nostalgic style, airy modern style, and professional style to metropolitan minimalist style. We provide support in every detail, including coloring selection of a piece of furniture, construction material selection, lighting selection, electricity system and many more. No matter what the space needs, what the project size or complexity, our team is able to communicate with our clients at every stage offering many dynamic ways to turn constraints into creative designs to fulfill all the functional requirements.
            </p>
        </div>
        <div class="col-md-6">
            <img src="images/interior_design.png" alt="interior-design" class="img-fluid">
        </div>
    </div>
</div>
<?php
@include ("inc/footer.php");
?>
