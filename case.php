<?php 
    @include("inc/header.php");
?>

<section class="case-back">
    <div class="container">
                <div class="title-text">
                    <h1>Marketing</h1>
                </div>
    </div>
</section>
<div class=" case-hendling container">
		<h2 class="color-title text-center my-5">Case Studies</h2>
				<p>
					To corporations, good branding can build a shared sense of trust and connection with their customers. Intelli Group helps our clients to identify their products and services by featuring their unique places in the markets. Not only can our professionalism add value to their businesses but also create sentimental attachments to customers as well as to business partners with their brands.
				</p>
				<p>
					Our branding designers are specialized in creating energizing works and services to redefine or enhance the images of our clients. We satisfy clients with branding designs that can describe their identities and concurrently remind their customers of the memorable components they experienced when they interact with the businesses. When we set out to establish new branding and marketing strategy, we integrate designs of logo, website, packaging and promotional materials of various functions with company image and values. Our marketing team, with strong branding strategies and distribution channels, enable us to make appropriate decisions on how, what, where, and when to deliver the brand message to the targeted customers.
				</p>
		<h2 class="color-title text-center my-5">Restaurants</h2>
				<p>
					We worked with various types of restaurant. Whether it serves Western, fusion or Asian cuisine, formal dining or casual dining, we helped our clients to gain popularity in the competitive market and achieve expanding client base through seamlessly integrated marketing packages and various promotional materials.
				</p>
				<div class="row">
					<div class="col-md-6">
						<img src="images/case-study_sushi_eh.png" alt="marketing" class="img-fluid" >
					</div>
					<div class="col-md-6">
						<img src="images/case-study_capstone.png" alt="marketing" class="img-fluid" >
					</div>
				</div>
		<h2 class="color-title text-center my-5">Products</h2>	
			<p>
				We have stepped into different industries and helped to market a wide range of products, from high-tech products to farm-produce. By conducting intensive marketing researches, designing marketing plans and materials, and managing client bases, we helped our clients to expand in both local and foreign markets, and build strong connections with various stakeholders.
			</p>
			<div class="row">
				<div class="col-md-6">
					<img src="images/case-study_ecs.png" alt="marketing" class="img-fluid">
				</div>
				<div class="col-md-6">
					<img src="images/case-study_wingtat.png" alt="marketing" class="img-fluid">
				</div>
			</div>

		<h2 class="color-title text-center my-5">Real Estate</h2>
			<p>
				Our marketing team has taken part in many large-scale real estate projects, including both residential and commercial projects. By providing marketing support in different stages from planning, construction, management to sale, we helped our clients to gain reputation and increase their presence in the market.
			</p>
			<div class="row">
				<div class="col-md-6">
					<img src="images/case-study_arista.png" alt="marketing" class="img-fluid">
				</div>
				<div class="col-md-6">
					<img src="images/case-study_trillium.png" alt="marketing" class="img-fluid">
				</div>
			</div>
</div>


<?php
@include("inc/footer.php");
?>