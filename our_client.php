<?php 
    @include("inc/header.php");
?>

<div class="container" id="our_client">
	<section class="our_client-back">
    <div class="container">
        <div  class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h1>Clients</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<h2 class="color-title text-center my-5">Our Client</h2>
<div class="client-col" style="margin-left: 15px; width: 92%; margin-left: 4%;">
	<p>
		Intelli Group takes pride in delivering all-around services to clients. Our clients come from over 100 different fields of business and industry, ranging from government agencies and transnational corporations to franchised coffee shops and local bakeries.
	</p>
	<div id="client">
	<div class="row">

		<div class="col-md-3" class="text-center">
			<img src="images/logo-aj-seafood.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-angela-dance.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-arista.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-bc-orchard.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-capstone.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-ccca.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-ccmm.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-couture.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-cte.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-derose.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-desert-hills.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-dr-bedy-lau.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-earthking.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-ecs.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-euroke.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-federal-entech.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-finns-kerrisdale-ladies.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-hkpr.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-hongkee.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-hotpot-and-bbq.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-house-of-canton.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-liberal.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-loyal.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-luxury-boutique.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-ma-japan.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-net-8.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-peekaboo.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-realtor.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-richbay.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-scwh.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-seafood-kingdom.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-sine-tamer.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-soho.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-state-oil.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-sushi-el.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-tipsy.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-togo.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-trillium-living.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-universal.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-urban-vibe.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-ursa-major.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-vanice-club.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-wedopia.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-wherels.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-wingtat.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-xinlilai.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-xo-beef-noodle.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-yalong.png" alt="clients">
		</div>

		<div class="col-md-3" class="text-center">
			<img src="images/logo-zhenjiang.png" alt="clients">
		</div>
	</div>
</div>
</div>
</div>





		





<?php
@include("inc/footer.php");
?>