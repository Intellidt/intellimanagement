<?php
@include ("inc/header.php");
?>
<section class="graphic-back">
    <div class="container">
        <div  class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h1>Design Solution</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <h2 class="color-title text-center my-5">Graphic Design</h2>
    <p class="text-left">
        Graphic design is the use of different elements to deliver aesthetic expression of concepts as a means to communicate with the audience in a visual way. Based on simple resources, our graphic designers create infinite variety. They use simple components like lines (straight, curved, wavy), shapes (square, circle, triangle, irregular), colors (bright, gradual, dim, shadow), and properties (font, spacing, alignment, size) to bring limitless possibilities. In the light of clients’ business needs and project requirements, they present clients with the designs that perfectly resonates with their corporate messages.            
    </p>
    <!-- Nav Tab -->
    <div class="text-center">
        <ul class="nav nav-pills mb-3 d-inline-flex" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-logo-tab" data-toggle="pill" href="#pills-logo" role="tab" aria-controls="pills-logo" aria-selected="true">Logo Design</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-stationary-tab" data-toggle="pill" href="#pills-stationary" role="tab" aria-controls="pills-stationary" aria-selected="false">Stationary</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-brochure-tab" data-toggle="pill" href="#pills-brochure" role="tab" aria-controls="pills-brochure" aria-selected="false">Brochure</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-advertisement-tab" data-toggle="pill" href="#pills-advertisement" role="tab" aria-controls="pills-advertisement" aria-selected="false">Advertisement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-poster-tab" data-toggle="pill" href="#pills-poster" role="tab" aria-controls="pills-poster" aria-selected="false">Poster</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-menu-tab" data-toggle="pill" href="#pills-menu" role="tab" aria-controls="pills-menu" aria-selected="false">Menu</a>
        </li>
        </ul>
    </div>
<div class="tab-content" id="pills-tabContent">
    <!-- LOGO MODAL -->
    <div class="tab-pane fade show active" id="pills-logo" role="tabpanel" aria-labelledby="pills-logo-tab">
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-angela-dance.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-arista.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/logo-bc-orchard.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-canvest-club.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-cbe.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="4">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-ccmm.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="5">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-cte.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="6">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-derose.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="7">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-earthking.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="8">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-ecs.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="9">
            </div>
            <div class="col-md-3">
                <img src="images/logo-euroke.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="10">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-luxury-boutique.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="11">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-federal-entech.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="12">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-global-synergy.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="13">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-house-of-canton.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="14">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-iguoyu.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="15">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-ma-japan.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="16">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-peekaboo.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="17">
            </div>
            <div class="col-md-3">
                <img src="images/logo-couture.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="18">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-posh-baby.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="19">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-seafood-kingdom.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="20">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-soho.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="21">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-sushi-el.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="22">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-tipsy.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="23">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-togo.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="24">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-trillium-living.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="25">
            </div>
            <div class="col-md-3">
                <img src="images/logo-universal.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="26">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-urban-vibe.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="27">
            </div>
        </div>
        <div class="row text-center mb-5" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-vanice-club.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="28">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-wherels.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="29">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-xinlilai.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="30">
            </div>
            <div class="col-md-3">
                    <img src="images/logo-iguoyu.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="31">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#logoModal">
            <div class="col-md-3">
                    <img src="images/logo-yalong.png" class="img-responsive" data-target="#carouselLogo" data-slide-to="32">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="modal fade" id="logoModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouselLogo" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/portfolio/logo_2.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_3.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_4.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_7.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_17.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_8.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_5.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_11.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_12.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_13.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_14.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_15.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_16.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_18.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_19.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_1.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_20.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_21.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_22.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_23.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_24.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_25.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_26.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_27.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_28.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_29.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_30.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_31.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_32.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_33.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_34.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_35.jpg">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/portfolio/logo_36.jpg">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselLogo" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselLogo" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- STATIONARY MODAL -->
    <div class="tab-pane fade" id="pills-stationary" role="tabpanel" aria-labelledby="pills-stationary-tab">
        <div class="row text-center mb-5" data-toggle="modal" data-target="#stationaryModal">
            <div class="col-md-3">
                    <img src="images/stationary-arista-small.png" class="img-responsive" data-target="#carouselStationary" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/stationary-ccmm-small.png" class="img-responsive" data-target="#carouselStationary" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/stationary-ecs-small.png" class="img-responsive" data-target="#carouselStationary" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/stationary-wingtat-small.png" class="img-responsive" data-target="#carouselStationary" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#stationaryModal">
            <div class="col-md-3">
                    <img src="images/stationary-wherels-small.png" class="img-responsive" data-target="#carouselStationary" data-slide-to="4">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="modal fade" id="stationaryModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouselStationary" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/stationary-arista.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/stationary-ccmm.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/stationary-ecs.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/stationary-wingtat.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/stationary-wherels.png">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselStationary" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselStationary" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BROCHURE MODAL -->
    <div class="tab-pane fade" id="pills-brochure" role="tabpanel" aria-labelledby="pills-brochure-tab">
        <div class="row text-center mb-5" data-toggle="modal" data-target="#brochureModal">
            <div class="col-md-3">
                    <img src="images/tbn-brochure-capstone.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn-brochure-ecs.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/tbn-brochure-peschisolido.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn-brochure-sime-famer.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#brochureModal">
            <div class="col-md-3">
                    <img src="images/tbn-brochure-trillium-living.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="4">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn-brochure-wingtat.png" class="img-responsive" data-target="#carouselBrochure" data-slide-to="5">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="modal fade" id="brochureModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouselBrochure" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/brochure-capstone.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/brochure-ecs.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/brochure-peschisolido.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/brochure-sime-famer.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/brochure-trillium-living.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/brochure-wingtat.png">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselBrochure" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselBrochure" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ADVERTISEMENT MODAL -->
    <div class="tab-pane fade" id="pills-advertisement" role="tabpanel" aria-labelledby="pills-advertisement-tab">
    <div class="row text-center mb-5" data-toggle="modal" data-target="#advertisementModal">
            <div class="col-md-3">
                    <img src="images/tbn_advertisement_1.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_advertisement_2.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_advertisement_3.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_advertisement_4.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#advertisementModal">
            <div class="col-md-3">
                    <img src="images/tbn_advertisement_5.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="4">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_advertisement_6.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="5">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_advertisement_7.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="6">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_advertisement_8.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="7">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#advertisementModal">
            <div class="col-md-3">
                <img src="images/tbn_advertisement_9.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="8">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_advertisement_10.png" class="img-responsive" data-target="#carouseladvertisement" data-slide-to="9">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="modal fade" id="advertisementModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouseladvertisement" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/advertisement_1.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_2.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_3.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_4.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_5.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_6.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_7.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_8.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_9.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/advertisement_10.png">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouseladvertisement" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouseladvertisement" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- POSTER MODAL -->
    <div class="tab-pane fade" id="pills-poster" role="tabpanel" aria-labelledby="pills-poster-tab">
        <div class="row text-center mb-5" data-toggle="modal" data-target="#posterModal">
            <div class="col-md-3">
                    <img src="images/tbn_poster_capstone.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_poster_capstone_2.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_poster_clf.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_poster_wingtat.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#posterModal">
            <div class="col-md-3">
                    <img src="images/tbn_poster_claypot.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="4">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_poster_scwh.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="5">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_poster_sing.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="6">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_poster_tantric.png" class="img-responsive" data-target="#carouselPoster" data-slide-to="7">
            </div>
        </div>
        <div class="modal fade" id="posterModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouselPoster" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/poster_capstone.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_capstone_2.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_clf.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_wingtat.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_claypot.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_scwh.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_sing.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/poster_tantric.png">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselPoster" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselPoster" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MENU MODAL -->
    <div class="tab-pane fade" id="pills-menu" role="tabpanel" aria-labelledby="pills-menu-tab">
        <div class="row text-center mb-5" data-toggle="modal" data-target="#menuModal">
            <div class="col-md-3">
                    <img src="images/tbn_menu_capstone.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="0">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_menu_house-of-canton.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="1">
            </div>
            <div class="col-md-3">
                <img src="images/tbn_menu_mars.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="2">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_menu_sushi-eh.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="3">
            </div>
        </div>
        <div class="row text-center" data-toggle="modal" data-target="#menuModal">
            <div class="col-md-3">
                    <img src="images/tbn_menu_manzo_1.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="4">
            </div>
            <div class="col-md-3">
                    <img src="images/tbn_menu_manzo_2.png" class="img-responsive" data-target="#carouselMenu" data-slide-to="5">
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="carouselMenu" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <img class="d-block w-100" src="images/menu_capstone.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/menu_house-of-canton.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/menu_mars.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/menu_sushi-eh.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/menu_manzo_1.png">
                            </div>
                            <div class="carousel-item">
                            <img class="d-block w-100" src="images/menu_manzo_2.png">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselMenu" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselMenu" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
@include ("inc/footer.php");
?>
