<?php 
    @include("inc/header.php");
?>
<div class="breadcrumb-area pt-300 pb-300 bg_image--55">
	<div class="container">
				<div class="breadcrumb-inner text-center">
					<h2 class="heading-h2 font-60 text-white">Contact Us</h2>
				</div>
	</div>
</div>

<main class="page-content">
	<div class="Contact-modern pb--120">
		<div class="container">
			<div class="row align-items-end">
				<div class="col-lg-6 col-12 pr--50 ptb-md--80 ptb-sm--80">
					<div class="Contact-modern bg_color--18 space_dec--100 pt--120 pb--120 pl--60 pr--60">
						<div class="inner">
							<h2 class="text-white">Start a new project?</h2>
							<div class="classic-address text-left mt--60">
								<h4 class="heading-h4 text-white">Visit Our Studio at</h4><br>
								<div class="desc mt--15">
									<p class="bk_pra line-height-2-22 text-white">
										200 - 3071 No.5 RoadRichmond,
									</p>
									<br>
									<p class="text-white">BC, V6X 2T4,CANADA</p>
								</div>
							</div>
							<div class="classic-address text-left mt--60">
								<h4 class="heading heading-h4 text-white">Message Us</h4>
								<div class="desc mt--15 mb--30">
									<p class="bk_pra line-height-2-22 text-white">
										info@intellidt.com
										</p>
										<br>
										<p class="text-white"> +1 (778) 297-7108
										</p>
								</div>
								<div class="social-share social -transparent text-white">
									<a href="#">
										<i class="fab fa-facebook"></i>
									</a>
									<a href="#">
										<i class="fab fa-twitter"></i>
									</a>
									<a href="#">
										<i class="fab fa-instagram"></i>
									</a>
									<a href="#">
										<i class="fab fa-linkedin"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- contact form -->
				<div class="col-md-6 col-12 pl--50">
					<div class="contact-form">
						<form id="contact-form" action="#" method="post">
							<div class="row">
								<div class="col-lg-12">
									<input type="text" name="name" placeholder="Name *" required>
								</div>
								<div class="col-lg-12 mt--30">
									<input type="email" name="email" placeholder="Email *" required>
								</div>
								<div class="col-lg-12 mt--30">
									<input type="text" name="phone" placeholder="Number *" required>
								</div>
								<div class="col-lg-12 mt--30">
									<textarea name="description" placeholder="Your Message *"></textarea>
								</div> 
								<div class="col-lg-12 mt--30">
									<input type="submit" value="submit" name="send">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
if(isset($_REQUEST['send'])) {
    $subject = "Website Inquiry";
    $message = $_POST['description'];
    $name=$_POST['name'];
    $phone=$_POST['phone'];
    $email2=$_POST['email'];
    $email = "info@intellidt.com";
    $headers = 'From:' . $email2 . "\r\n"; // Sender's Email
    $message = 'Name: '. $name."\r\n".'Email: '. $email2."\r\n".'Phone: '. $phone."\r\n".'Message: '.wordwrap($message, 70);
    $data = mail($email, $subject, $message, $headers);
}

?>

<?php
@include("inc/footer.php");
?>
