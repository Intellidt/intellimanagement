<?php
@include ("inc/header.php");
?>
<section class="interior-back">
    <div class="container">
        <div  class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h1>Design Solution</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <h2 class="color-title text-center my-5">Marketing Campaign</h2>
    <div class ="row">
        <div class="col-md-12">
            <p class="text-left">
                Good marketing campaign can effectively promote a product, service, or company through a series of promotion procedures that integrates advertising, demonstration, marketing and promotion events, and other interactive techniques. The professionals from the Intelli Group work behind the scenes to plan, implement, and track the marketing campaigns to ensure that their successes will lead to better business and sales performances.            </p>
        </div>
    </div>
</div>
<?php
@include ("inc/footer.php");
?>
