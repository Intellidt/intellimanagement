<?php 
    @include("inc/header.php");
?>
<section class="event-back">
    <div class="container">
                <div class="title-text">
                    <h1>Marketing</h1>
                </div>
    </div>
</section>
<div class=" event-hendling container">
	<h2 class="color-title text-center my-5">Event Planning</h2>
	<div class="row">
		<div class="col-md-6">
			<p>
				Like all other services we provide, our team of specialists in event planning works hand in hand with our clients. We are passionate in helping our clients to achieve their dreams and realize their goals, and this is what enables us to satisfy each project’s every particular need in a customized fashion.
			</p>
			<p>
				Experiences show us that communication is the key in creating unforgettable events. Our inspirations for innovative event concepts are always generated after communicating with our clients, through which we understand their event purposes and target audiences as well as the nature and culture of their businesses or affiliations. Once the event concepts are captured and the designs are selected, we develop and execute the plans with assurance that the events successfully attain their goals and reach out to connect our clients with their target and potential audiences.
			</p>
			<p>
				Our team’s talents have been fully demonstrated in a variety of types of events ranging from festivals, ceremonies, charity functions and educational workshops to commercial promotions and entertainment events. Services that we delivered are quality, meticulous, and comprehensive – budgeting, scheduling, site selection, promotion, stage design and construction, permits application, onsite coordination, guest and performer arrangements, security arrangement and catering plan, and more.
			</p>
			<p>
				Memorable events that we have designed, produced, and coordinated include, but are not limited to:
			</p>
			<ul>
				<li>Charity fundraising event for a society of a special theme museum</li>
				<li>World tour instruction workshop of a well-known Tai Chi Master</li>
				<li>Tour concert in Vancouver of a group of famous singers from Hong Kong</li>
				<li>New Year's Event for the largest Asian-theme Shopping Mall in Vancouver</li>
				<li>Indoor and outdoor commercial performances in public arenas across different cities in the Greater Vancouver area</li>
			</ul>
		</div>
		<div class="col-md-6">
			<img src="images/marketing_event.png" alt="marketing" class="img-fluid" >
		</div>
	</div>
</div>




<?php
@include("inc/footer.php");
?>
