<?php
    @include("inc/header.php");
?>
<div id="carouselExampleControls" class="carousel vert slide" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active ">
            <div class="slider1">
                <div class="polygonshape">
                </div>
                <div class="slider-detail w-100">
                    <h2 class="text-center font-weight-bold">Bringing the best digital solutions for your brands</h2>
                </div>  
            </div>  
        </div>
        <div class="carousel-item">
            <div class="slider2">
                <div class="polygonshape1">
                </div>
                <div class="slider-detail w-100">
                    <h2 class="text-center font-weight-bold">Bringing the best digital solutions for your brands</h2>
                </div>
            </div>    
        </div>
        <div class="carousel-item">
            <div class="slider3">
                <div class="polygonshape">
                </div>
                <div class="slider-detail w-100">
                    <h2 class="text-center font-weight-bold">Bringing the best digital solutions for your brands</h2>
                </div>
            </div>    
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bg-dark rounded-circle" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon bg-dark rounded-circle" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-6">
            <div class="who-we-are mt-5 text-left">
                    <h6 class="color-title py-5">Who We Are</h6>
                    <div class="home_quote mb-5">
                        <img src="images/home_quote.png" alt="quote" class="img-fluid">
                    </div>
                    <p>Intelli teams up top-grade design professionals, marketing specialists, and branding strategists to serve clients across a variety of industries. We provide exclusive designs and smart marketing solutions of high standards of excellence.</p>
                    <p>What we focus is to facilitate business growth, profitability, and opportunity. We communicate with clients, strategizing and planning together with them. That allows us to create the right platform, target the right people, and deliver the right message, at the right time and in the right place.</p>
                    <p>Our customized designs are presented with best tactics and technologies to deliver strong results. We integrate both conventional and contemporary strategies and techniques to implement effective and innovative marketing and branding campaigns. The quality of our work successfully elevates our clients above the increasingly competitive market, helping them to win customers, gain loyalty, and maximize long-term impact and returns.</p>
                    <p>In addition to full design services, we also specialize in rebrand marketing initiatives and corporate multimedia presentations. With recognized expertise in graphic design, web development, interior design, social media, branding, and marketing, we have assisted many local and oversea companies to enhance their businesses and generate sustainable corporate identity.</p>
            </div> 
        </div> 
        <div class="col-md-6 mt-0 space">
            <div class="">
                <img src="images/who-we-are-image.jpg" class="img-fluid">
            </div> 
        </div>
    </div>     

        <div class="our-services mt-5 space">
            <h6 class="color-title text-center mb-5">Portfolio</h6>
            <h2 class="text-center mb-5 space">Collection of the greatest works.</h2>
            <div class="row mt-4">
                <div class="col-md-4 text-center space">
                    <div class="service-block mx-5">
                        <a href="graphic_design.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="graphic_design.php" class="service-heading">
                            <h5 class="mt-3">Graphic Design</h5>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 text-center space">
                    <div class="service-block mx-5">
                        <a href="web_development.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="web_development.php" class="service-heading">
                            <h5 class="mt-3">Web Development</h5>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 text-center space">
                    <div class="service-block mx-5">
                        <a href="graphic_design.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="graphic_design.php" class="service-heading">
                            <h5 class="mt-3">Graphic Design</h5>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-4 text-center">
                    <div class="service-block mx-5">
                        <a href="marketing_campaign.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="marketing_campaign.php" class="service-heading">
                            <h5 class="mt-3">Marketing Campaign</h5>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="service-block mx-5">
                        <a href="interior_design.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="interior_design.php" class="service-heading">
                            <h5 class="mt-3">Interior Design</h5>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="service-block mx-5">
                        <a href="marketing_campaign.php">
                            <img src="images/service.jpg" class="img-fluid">
                        </a>
                        <a href="marketing_campaign.php" class="service-heading">
                            <h5 class="mt-3">Marketing Campaign</h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="best-services mt-5 space">
            <h2 class="color-title text-center mb-5">Best Services</h2>
            <p class="text-left space">
            Brook presents your services with flexible, convenient and multipurpose layouts. You can select your favorite layouts & elements for particular projects with unlimited customization possibilities. Pixel-perfect replication of the designers is intended for both front-end & back-end developers.
            </p>
        </div>

        <div class="reasons-why mt-5 jumbotron space">
            <h6 class="color-title text-center mb-5">REASONS WHY</h6>
            <h2 class="text-center mb-5">We're trusted by clients.</h2>
            <div class="row mt-4">
                <div class="col-md-4 text-center">
                    <div class="reson-why-block mx-5">
                        <i class="far fa-eye fa-3x" style="color: #ce8f4f"></i>
                        <h5 class="mt-3">Modern design</h5>
                        <p class="mt-2">
                        Brook embraces a modern look with various enhanced pre-defined page elements.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="reson-why-block mx-5">
                        <i class="far fa-bookmark fa-3x" style="color: #ce8f4f"></i>
                        <h5 class="mt-3">UI/UX designs</h5>
                        <p class="mt-2">
                        We successfully implemented numerous UI/UX projects for both global & local clients.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="reson-why-block mx-5">
                        <i class="far fa-window-restore fa-3x" style="color: #ce8f4f"></i>
                        <h5 class="mt-3">SEO marketing</h5>
                        <p class="mt-2">
                        Brook is highly responsive thanks to built-in WP Bakery Page Builder & Slider Revolution.
                        </p>
                    </div>
                </div>
            </div>
        </div>

</div>
<?php
@include("inc/footer.php");
?>
