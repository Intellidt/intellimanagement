<?php 
    @include("inc/header.php");
?>

<section class="web_development-back">
    <div class="container">
        <div  class="row">
            <div class="col-md-12">
                <div class="title-text">
                    <h1>Design solutions</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<div class=" case-hendling container">
<h2 class="color-title text-center my-5">Website Development</h2>
<div class="row">
		<div class="line">
			<p>
				For many businesses, website is the first contact point for their customers, suppliers, and business partners. The need for a strong online presence is even important for customers who want to quickly understand a company or its product. Our web development team always stays one step ahead in the web trend and is well-prepared to deliver our comprehensive services. Customized solutions are specifically designed to fully, clearly, and concisely define our clients’ mission concepts, with the purpose of helping them to achieve their potentials.
			</p>
			<p>
				Our service includes, but are not limited to:
			</p>
			<div class="web">
				<div class="web-1">
					<ul>
						<li>Website Development</li>
						<li>Wordpress Websites</li>
						<li>Responsive Design</li>
					</ul>
				</div>
				<div class="web-1">
					<ul>
						<li>Custom Web Design</li>
						<li>Domain and Hosting</li>
						<li>E-commerce</li>
					</ul>
				</div>
				<div class="web-1">
					<ul>
						<li>Content Management</li>
						<li>Online Marketing</li>
						<li>Web Applications</li>
					</ul>
				</div>
			</div>





<?php
@include("inc/footer.php");
?>